import SinglePost from '../SinglePost';
// type post = {
// 	email: string;
// 	likes: number;
// 	title: string;
// 	description: string;
// 	image: String;
// 	messages: Message;
// };
// type Message = {
// 	user: string;
// 	postId: string;
// 	message: string;
// 	intent: 'chat';
// 	//add date
// };

export default function Post(props: any) {
	return (
		<div>
			<div>
				{props.posts
					? props.posts.map((post: any, index: any) => <SinglePost post={post} key={index} />)
					: 'no posts yet'}
			</div>
		</div>
	);
}
