import Navigation from '../Navigation';
import { Flex } from '@chakra-ui/react';

const Header = (props: any) => {
	return (
		<div>
			<Flex w="100%" align="center" justify="center">
				<Navigation Buttons={props} />
			</Flex>
		</div>
	);
};
export default Header;
