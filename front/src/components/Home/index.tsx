import './Home.css';
import Feed from '../Feed';
import Profile from '../Profile';
import WelcomUser from '../WelcomUser';
import { useEffect, useState, useRef, forwardRef } from 'react';
import { FaFacebook } from 'react-icons/fa';
import Header from '../Header';
import News from '../News';
import { TwitterShareButton } from 'react-share';
import { AiFillLinkedin } from 'react-icons/ai';
import Footer from '../Footer';
import { useNavigate } from 'react-router-dom';
import ContactForm from '../ContactForm';

import banner1 from '../../resources/banner1.png';
import banner2 from '../../resources/banner2.png';
import banner3 from '../../resources/banner3.png';
import banner4 from '../../resources/banner4.png';

import {
	useColorModeValue,
	Grid,
	GridItem,
	Flex,
	StackItem,
	Avatar,
	AvatarBadge,
	HStack,
	VStack,
	Box,
	Image,
	Text,
	Link,
} from '@chakra-ui/react';

export default function Home() {
	const section = useRef(null);
	const navigate = useNavigate();
	const [logged, setLogged] = useState(false);
	const [email, setEmail] = useState(''); //username its not email
	const [showprofile, setShowprofile] = useState(false);

	const [showForum, setshowForum] = useState(false);
	const [showContact, setShowContact] = useState(false);
	const [alluserInfo, setalluserInfo] = useState({});
	const [userName, setUserName] = useState('');
	const [avatar, setAvatar] = useState('');

	const [showComp, setShowComp] = useState('News');

	const scrollToSection = () => {
		console.log('shouldscrooll');
		//@ts-ignore
		section.current.scrollIntoView({ block: 'start', behavior: 'smooth' });
		console.log('shouldscrooll');
	};

	function logOut() {
		localStorage.clear();
		setLogged(true);
		navigate('/');
	}

	function showprofilefunc() {
		scrollToSection();

		setShowprofile(!showprofile);
	}

	function showForumfunc() {
		scrollToSection();

		setshowForum(!showForum);
	}

	function showCompfunc(comp: any) {
		scrollToSection();
		setShowComp(comp);
	}

	async function getTokenInfo() {
		let token = localStorage.token;
		if (token) {
			const tokenParts = token.split('.');
			const encodedPayload = tokenParts[1];
			const rawPayload = atob(encodedPayload);
			const user = JSON.parse(rawPayload);
			return user.username;
		}
	}

	const userInfo = () => {
		fetch('/api/getUserInfo', {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
				'x-access-token': localStorage.getItem('token') || '', //check if we have jtw token which is created after each log-in
				token: localStorage.token || '',
				Accept: 'application/json',
			},
		})
			.then((res) => res.json())
			.then((data) => {
				if (data) {
					setalluserInfo({ data });
					setAvatar(data.avatar);
					setUserName(data.username);
				} else setalluserInfo({});
				console.log(data);
			})
			.catch((error) => console.log(error));
	};

	useEffect(() => {
		const getEmail = async () => {
			const userEmail = await getTokenInfo();
			setEmail(userEmail);
		};
		getEmail();
		if (localStorage.token) {
			userInfo();
		}
	}, [logged]);

	return (
		<div>
			<div>
				<Grid
					h="550px"
					templateRows="repeat(11, 1fr)"
					templateColumns="repeat(2, 1fr)"
					gap={3}
					py={0}
				>
					<GridItem colSpan={2} rowSpan={6} className="bg">
						<VStack spacing={0} align="stretch">
							<Box h="243px" p={5}>
								<HStack alignItems="right" justify="right" h="50px">
									<StackItem>
										{/* <Heading size="md">The Seals are calling you...</Heading> */}
										<div>
											{localStorage.token ? (
												<div>
													<HStack>
														<Text fontWeight="medium">Welcome {userName}</Text>
														<Avatar src={avatar}>
															<AvatarBadge boxSize="1.25em" bg="green.500" />
														</Avatar>
													</HStack>
												</div>
											) : null}
										</div>
									</StackItem>
								</HStack>
							</Box>

							<GridItem>
								<Box h="60px" p={1}>
									<HStack alignItems="right" justify="right" h="30px">
										<StackItem>
											<Header
												email={email}
												logOut={logOut}
												showProfile={showprofilefunc}
												showProfilestate={showprofile}
												showForumfunc={showForumfunc}
												showForum={showForum}
												showComp={showCompfunc}
											></Header>
										</StackItem>
									</HStack>
								</Box>
							</GridItem>
						</VStack>
					</GridItem>
					<GridItem colSpan={2} rowSpan={1}>
						{/* <Text fontSize="20px" align="center">
						Check out sister websites
					</Text> */}
					</GridItem>

					{/* <GridItem colSpan={6} rowSpan={2} bg="papayawhip" /> */}
					<GridItem colSpan={1} rowSpan={2} bg={useColorModeValue('gray.200', 'gray.600')}>
						<Image
							boxSize="100%"
							src={banner1}
							objectFit="-moz-initial"
							display="block"
							opacity="1"
							p="1px"

							// transition="ease-in"
						/>
					</GridItem>
					<GridItem colSpan={1} rowSpan={2} bg={useColorModeValue('gray.200', 'gray.900')}>
						<Image
							boxSize="100%"
							src={banner2}
							objectFit="-moz-initial"
							display="block"
							opacity="1"
							p="1px"

							// transition="ease-in"
						/>
					</GridItem>
					<GridItem colSpan={1} rowSpan={2} bg={useColorModeValue('gray.200', 'gray.900')}>
						<Image
							boxSize="100%"
							src={banner3}
							objectFit="-moz-initial"
							display="block"
							opacity="1"
							p="1px"

							// transition="ease-in"
						/>
					</GridItem>
					<GridItem colSpan={1} rowSpan={2} bg={useColorModeValue('gray.200', 'gray.900')}>
						<Image
							boxSize="100%"
							src={banner4}
							objectFit="-moz-initial"
							display="block"
							opacity="1"
							p="1px"

							// transition="ease-in"
						/>
					</GridItem>
				</Grid>
				<div ref={section}>
					<Flex alignItems="center" justify="center">
						{showComp === 'News' ? <News /> : null}

						{showComp === 'profile' ? (
							<>
								{' '}
								{localStorage.token ? (
									<Flex>
										<Profile userInformation={alluserInfo} />
									</Flex>
								) : null}
							</>
						) : null}

						{/* {showComp === 'feed' && localStorage.token ? (
					<> {localStorage.token ? <Feed /> : null}</>
				) : null} */}
						{showComp === 'feed' && <Feed />}
					</Flex>
				</div>
				<div>{showComp === 'contact' && <ContactForm />}</div>
			</div>
			<Footer />
		</div>
	);
}
