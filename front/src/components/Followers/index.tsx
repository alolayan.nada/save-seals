import Follower from '../Follower';
import {
	Menu,
	MenuButton,
	MenuList,
	MenuItem,
	MenuGroup,
	MenuDivider,
	Button,
} from '@chakra-ui/react';

import './Followers.css';

export default function Followers(props: any) {
	const numberOfFriends = props.userFollower.length;
	return (
		<div>
			<Menu closeOnSelect={false}>
				<MenuButton as={Button} colorScheme="gray">
					Friends: {numberOfFriends}
				</MenuButton>
				<MenuList minWidth="240px">
					<MenuGroup defaultValue="asc">
						{props.userFollower.map((Followe: any) => (
							<MenuItem
								value="asc"
								id={Followe.userid}
								onClick={() => props.isClickedfunc(Followe)}
							>
								{' '}
								<Follower Followe={Followe} />
							</MenuItem>
						))}
					</MenuGroup>
					<MenuDivider />
				</MenuList>
			</Menu>
		</div>
	);
}
