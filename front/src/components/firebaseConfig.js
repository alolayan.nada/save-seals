import firebase from 'firebase/compat/app';

import 'firebase/compat/storage';
require('dotenv').config();
//console.log(process.env.FIREBASE_APIKEY,process.env.FIREBASE_AUTHDOMAIN,process.env.FIREBASE_PROJECTID,process.env.FIREBASE_STORAGEBUCKET,process.env.FIREBASE_MESSAGINGSENDERID,process.env.FIREBASE_MESSAGINGSENDERID,process.env.FIREBASE_APPID,process.env.FIREBASE_MEASUREMENTID)
<script src="https://www.gstatic.com/firebasejs/5.9.1/firebase-storage.js"> </script>;
const firebaseConfig = {
	apiKey: process.env.FIREBASE_APIKEY,
	authDomain: process.env.FIREBASE_AUTHDOMAIN,
	databaseURL: process.env.REACT_APP_DB,
	projectId: process.env.FIREBASE_PROJECTID,
	storageBucket: process.env.FIREBASE_STORAGEBUCKET,
	messagingSenderId: process.env.FIREBASE_MESSAGINGSENDERID,
	appId: process.env.FIREBASE_APPID,
	measurementId: process.env.FIREBASE_MEASUREMENTID,
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);
const storage = firebase.storage();

export { storage, firebase as default };

// // Create a root reference
// const storage = getStorage();

///
// import firebase from 'firebase/compat/app';

// import 'firebase/compat/storage';

// <script src="https://www.gstatic.com/firebasejs/5.9.1/firebase-storage.js"> </script>;
// require('dotenv').config();
// const firebaseConfig = {
// 	apiKey: process.env.FIREBASE_APIKEY,
// 	authDomain: process.env.FIREBASE_AUTHDOMAIN,
// 	projectId: process.env.FIREBASE_PROJECTID,
// 	storageBucket: process.env.FIREBASE_STORAGEBUCKET,
// 	messagingSenderId: process.env.FIREBASE_MESSAGINGSENDERID,
// 	appId: process.env.FIREBASE_APPID,
// 	measurementId: process.env.FIREBASE_MEASUREMENTID,
// };

// // Initialize Firebase
// firebase.initializeApp(firebaseConfig);
// const storage = firebase.storage();

// export { storage, firebase as default };
