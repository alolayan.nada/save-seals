import React from 'react';

const OuterLinks = () => {
	return (
		<div>
			<div className="text-wrapper">
				<div className="col-banner">col</div>
				<div className="col-left">col-left</div>
				<div className="textBox">
					<div className="box1">box1</div>
					<div className="box2">box2</div>
				</div>
			</div>
		</div>
	);
};

export default OuterLinks;
