// @ts-ignore
import { storage } from '../firebaseConfig';
import { useState } from 'react';
import { Progress } from '@chakra-ui/react';

import './ImageUpload.css';

const ImageUpload = (props: any) => {
	const [image, setImage] = useState<any>(null);
	const [url, setUrl] = useState<any>('');
	const [progress, setProgress] = useState(0);
	const [error, setError] = useState('');

	function onInputChange(e: any) {
		console.log(e.target.files[0], 'file');
		const file = e.target.files[0];

		if (file) {
			const fileType = file.type;
			const validImageTypes = ['image/gif', 'image/jpeg', 'image/png'];
			if (validImageTypes.includes(fileType)) {
				handleUpdate(file);
			} else {
				setError('please upload a file');
			}
			console.log(image, 'why');
		}
	}

	function handleUpdate(image: any) {
		if (image) {
			// add to image folder in firebase
			const uploadTask = storage.ref(`images/${image.name}`).put(image);
			// Listen for state changes, errors, and completion of the upload.
			uploadTask.on(
				'state_changed',
				(snapshot: any) => {
					// Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
					const progress = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
					setProgress(progress);
				},
				(error: any) => {
					// error function ....
					console.log(error);
					setError(error);
				},
				() => {
					// complete function ....
					storage
						.ref('images')
						.child(image.name) // Upload the file and metadata
						.getDownloadURL() // get download url
						.then((url: any) => {
							console.log('inside url');
							props.geturl(url);

							setUrl(url);

							setProgress(0);
							setImage(image);
						});
				}
			);
		} else {
			setError('Error please choose an image to upload');
		}
	}

	return (
		<div>
			<div>
				<label className="custom-file-upload">
					<input type="file" onChange={onInputChange} />
					Upload avatar
				</label>
			</div>

			{/* </form> */}

			<div style={{ height: '50px' }}>
				{progress > 0 ? (
					<Progress size="xs" isIndeterminate>
						{' '}
						please wait for the upload then submit
					</Progress>
				) : (
					''
				)}
				<p style={{ color: 'red' }}>{error}</p>
			</div>
		</div>
	);
};

export default ImageUpload;
