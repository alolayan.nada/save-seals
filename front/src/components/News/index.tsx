import {
	Box,
	Grid,
	GridItem,
	Flex,
	Center,
	Heading,
	Text,
	useColorModeValue,
	Image,
	VStack,
	StackDivider,
} from '@chakra-ui/react';

import pita from '../../resources/pita-intro.png';
import pita2 from '../../resources/pita-ryan.png';
import { TwitterShareButton } from 'react-share';
import { TwitterIcon } from 'react-share';

const News = () => {
	return (
		<div>
			<Center py={5}>
				<Box
					maxW={'70%'}
					w={'full'}
					bg={useColorModeValue('white', 'gray.900')}
					boxShadow={'2xl'}
					rounded={'md'}
					p={2}
					overflow={'hidden'}
				>
					<Grid
						h="-moz-max-content"
						templateRows="repeat(1, 1fr)"
						templateColumns="repeat(1, 1fr)"
						gap={1}
						p={2}
					>
						<GridItem
							colSpan={2}
							bg={useColorModeValue('gray.100', 'gray.200')}
							h="-moz-max-content"
						>
							<VStack
								divider={<StackDivider borderColor="gray.200" />}
								spacing={2}
								align="left"
								p={2}
							>
								<Box h="-moz-max-content">
									<Heading>
										<Flex
											padding="10px 10px 10px 10px"

											// border-top="1px"
											// fontSize="13px"
											// fontWeight="700"
											// background-color="var(--white-color)"
										>
											<Text>
												Many of these friendly gentle animals are still alive when they are skinned!
											</Text>
											<TwitterShareButton
												url={`https://seals-help.herokuapp.com/`}
												title="Check this out "
											>
												{' '}
												<TwitterIcon size={32} round={true} />
											</TwitterShareButton>
										</Flex>
									</Heading>
								</Box>

								<Box h="-moz-min-content" bg="">
									<Text fontSize="md">
										{' '}
										<a
											href="
											https://www.peta.org/blog/faces-of-seal-slaughter/"
										>
											{' '}
											&copy;https://www.peta.org/blog/faces-of-seal-slaughter/
										</a>
									</Text>
								</Box>

								<Flex
									w="-moz-min-content"
									// bg={useColorModeValue('white', 'gray.900')}
									justify="center"
									align="center"
								>
									<Image
										boxSize="100%"
										src={pita}
										objectFit="-moz-initial"
										display="block"
										opacity="1"
										p="1px"

										// transition="ease-in"
									/>
								</Flex>
							</VStack>
						</GridItem>
					</Grid>
				</Box>
			</Center>

			<Center py={5}>
				<Box
					maxW={'70%'}
					w={'full'}
					bg={useColorModeValue('white', 'gray.900')}
					boxShadow={'2xl'}
					rounded={'md'}
					p={2}
					overflow={'hidden'}
				>
					<Grid
						h="-moz-min-content"
						templateRows="repeat(1, 1fr)"
						templateColumns="repeat(1, 1fr)"
						gap={1}
						p={2}
					>
						<GridItem
							colSpan={1}
							bg={useColorModeValue('gray.100', 'gray.200')}
							h="-moz-min-content"
						>
							<VStack
								divider={<StackDivider borderColor="gray.200" />}
								spacing={2}
								align="left"
								p={2}
							>
								<Box h="-moz-max-content" bg="">
									<Heading mb={5}>
										<Flex
											//padding="10px 30px"
											border-top="1px"
											background-color="var(--white-color)"
										>
											<Text>
												Ryan Renynolds narrates A chilling film about Canada's seal slaughter
											</Text>
											<TwitterShareButton
												url={`https://seals-help.herokuapp.com/`}
												title="Check this out "
											>
												{' '}
												<TwitterIcon size={32} round={true} />
											</TwitterShareButton>
										</Flex>
									</Heading>
								</Box>
								<Box h="-moz-min-content" bg="">
									<Text fontSize="md">
										&copy;
										<a
											href="&copy;
												https://www.canadasshame.com/2016/10/19/ryan-reynolds-narrates-chilling-film-canadas-seal-slaughter/"
										>
											{' '}
											https://www.canadasshame.com/2016/10/19/ryan-reynolds-narrates-chilling-film-canadas-seal-slaughter/
										</a>
									</Text>
								</Box>

								<Flex
									w="-moz-min-content"
									// bg={useColorModeValue('white', 'gray.900')}
									justify="center"
									align="center"
								>
									<Image
										boxSize="100%"
										src={pita2}
										objectFit="-moz-initial"
										display="block"
										opacity="1"
										p="1px"

										// transition="ease-in"
									/>
								</Flex>
							</VStack>
						</GridItem>
					</Grid>
				</Box>
			</Center>
		</div>
	);
};

export default News;
