import { useState } from 'react';

import ImageUpload from '../ImageUpload';
import {
	Stack,
	InputGroup,
	Input,
	FormControl,
	Box,
	Center,
	useColorModeValue,
	Textarea,
	Image,
	Button,
} from '@chakra-ui/react';

export default function Posts(props: any) {
	const [likes, setlikes] = useState(0);
	const [title, setTitle] = useState('');
	const [description, setDescription] = useState('');
	const [messages, setmessages] = useState([]);
	const [image, setImage] = useState('');

	const setImagefun = async (image: any) => {
		console.log(image, 'imagee');
		setImage(image);
	};

	async function makePost(e: any) {
		console.log('clickedddd');
		e.preventDefault();
		if (!title || !description || !image) {
			console.log('enter fields');
		} else {
			console.log({ likes, title, description, messages, image }, 'make post');
			props.addPost({ likes, title, description, messages, image });
			setlikes(0);
			setTitle('');
			setDescription('');
			setmessages([]);
			setImage('');
		}
	}

	return (
		<div>
			<div>
				<Center py={0}>
					<Box
						maxW={'600px'}
						w={'full'}
						bg={useColorModeValue('white', 'gray.900')}
						boxShadow={'2xl'}
						rounded={'md'}
						p={6}
						overflow={'hidden'}
					>
						Post
						<form onSubmit={makePost}>
							<Stack spacing={2}>
								<FormControl isRequired>
									<InputGroup>
										<Input
											type="title"
											placeholder="Title"
											aria-label="Title"
											onChange={(e) => {
												setTitle(e.target.value);
											}}
										/>
									</InputGroup>
								</FormControl>
								<FormControl>
									<InputGroup>
										<Textarea
											placeholder="description"
											aria-label="description"
											rows={10}
											onChange={(e) => {
												setDescription(e.target.value);
											}}
										/>
									</InputGroup>
								</FormControl>
								<FormControl>
									<InputGroup>
										<ImageUpload geturl={setImagefun} />
										{image ? (
											<Image rounded={'lg'} boxSize="30%" src={image} objectFit={'cover'} />
										) : (
											'no Image'
										)}
									</InputGroup>
								</FormControl>
							</Stack>
							<Button type="submit">Submit</Button>
						</form>
					</Box>
				</Center>
			</div>
		</div>
	);
}
