// @ts-nocheck
import { useState, useEffect } from 'react';
import { apiCall } from '../../utility';
import {
	Box,
	useColorModeValue,
	Center,
	Heading,
	Image,
	Button,
	Grid,
	GridItem,
	Flex,
	VStack,
	StackDivider,
	Text,
} from '@chakra-ui/react';

export default function SinglePost(props: any) {
	const [email, setEmail] = useState(props.post.email);
	const [freindAddStatus, setFriendAddStatus] = useState(false);

	useEffect(() => {
		setFriendAddStatus(false);
	}, []);
	function showUser() {
		setEmail(props.post.email);
		idfromEmail(email);
	}

	async function idfromEmail(email: any) {
		//const res = await
		apiCall('/api/idfromEmail', { email }).then((id) => {
			addFollowing(id);
		});
	}

	async function addFollowing(id: any) {
		const url = '/api/follow/';
		const path = id;
		fetch(`${url}${path}`, {
			method: 'PATCH',
			headers: {
				'Content-Type': 'application/json',
				'x-access-token': localStorage.getItem('token') || '', //check if we have jtw token which is created after each log-in
				token: localStorage.token || '',
			},
		}).then((t) => setFriendAddStatus(true));
	}

	return (
		<div>
			<Center py={6}>
				<Box
					maxW={'85%'}
					w={'full'}
					bg={useColorModeValue('white', 'gray.900')}
					boxShadow={'2xl'}
					rounded={'md'}
					p={2}
					overflow={'hidden'}
				>
					<Grid
						h="520px"
						templateRows="repeat(2, 1fr)"
						templateColumns="repeat(5, 1fr)"
						gap={1}
						p={2}
					>
						<GridItem colSpan={4} bg={useColorModeValue('gray.100', 'gray.200')} h="500px">
							<VStack
								divider={<StackDivider borderColor="gray.200" />}
								spacing={2}
								align="left"
								p={2}
							>
								<Box h="50px" bg="">
									<Heading mb={10} isTruncated>
										{props.post.title}
										<Flex
											border-top="1px"
											fontSize="13px"
											fontWeight="700"
											background-color="var(--white-color)"
										>
											<time>{props.post.date}</time>
										</Flex>
									</Heading>
								</Box>

								<Box h="40px" bg="">
									<Text fontSize="md">{props.post.description}</Text>
								</Box>

								<Flex w="1000px" h="250px" justify="center" align="center">
									<Image
										boxSize="100%"
										src={props.post.image}
										objectFit="contain"
										display="block"
										opacity="1"
										pr="15%"

										// transition="ease-in"
									/>
								</Flex>
							</VStack>
						</GridItem>
						<GridItem
							rowSpan={2}
							colSpan={1}
							bg={useColorModeValue('gray.100', 'gray.200')}
							h="500px"
						>
							<Box>
								<Text fontWeight="bald" p="10px">
									{props.post.username}
								</Text>
							</Box>
							<Flex flex={1} bg="blue.200">
								<Image objectFit="cover" boxSize="100%" src={props.post.avatar} />
							</Flex>
							<Center>
								<Button onClick={showUser}>Add as Friend</Button>
								{freindAddStatus ? alert('Added as a friend') : ''}
							</Center>
						</GridItem>
					</Grid>
				</Box>
			</Center>
		</div>
	);
}
