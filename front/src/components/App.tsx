import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Home from './Home';
import Login from './Login';
import Register from './Register';
import Result from './Result';
import News from './News';

export default function App() {
	return (
		<div>
			{/* <div className="tcontainer">
				<div className="wrapper"> */}
			<Router>
				<Routes>
					<Route path="/" element={<Home />} />
					<Route path="/News" element={<News />} />
					<Route path="/login" element={<Login />} />
					<Route path="/register" element={<Register />} />
					<Route path="/result" element={<Result />} />
				</Routes>
			</Router>
		</div>
		// 	</div>
		// </div>
	);
}
