"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
// import { Grid, Paper, Avatar, TextField, Button, Typography } from '@material-ui/core';
// import { Link } from 'react-router-dom';
var react_1 = require("react");
var utility_1 = require("../../utility");
var react_router_dom_1 = require("react-router-dom");
// import AccountCircleIcon from '@material-ui/icons/AccountCircle';
// import './Login.css';
// export default function Login() {
// 	const paperStyle = { padding: 20, height: '70vh', width: 280, margin: '20px auto' };
// 	const avatarStyle = { backgroundColor: 'blue' };
// 	const buttonStyle = { margin: '10px 0' };
// 	const [email, setEmail] = useState('');
// 	const [password, setPassword] = useState('');
// 	const navigate = useNavigate();
// 	async function check(e: any) {
// 		e.preventDefault();
// 		const payload = { email: email, password: password };
// 		const res = await apiCall('/login', { email, password });
// 		if (res.status === 'ok') {
// 			localStorage.setItem('token', res.data); //I might change it to "refresh token"
// 			console.log(res.data, 'in front res.data');
// 			alert('You are logged in');
// 			navigate('/', { replace: true }); //the user is pushed to the feeed page once they are logged in
// 		} else {
// 			alert(res.error); //error property if login not successful
// 		}
// 	}
// 	return (
// 		<Grid>
// 			<Paper elevation={10} style={paperStyle}>
// 				<Grid container direction="column" justifyContent="center" alignItems="center">
// 					<Avatar style={avatarStyle}>
// 						<AccountCircleIcon />
// 					</Avatar>
// 					<h2>Login</h2>
// 					<form onSubmit={check}>
// 						<TextField
// 							label="email"
// 							value={email}
// 							placeholder="Enter Email"
// 							fullWidth
// 							required
// 							onChange={(e) => setEmail(e.target.value)}
// 						></TextField>
// 						<TextField
// 							label="password"
// 							placeholder="Enter Password"
// 							value={password}
// 							type="password"
// 							fullWidth
// 							required
// 							onChange={(e) => setPassword(e.target.value)}
// 						></TextField>
// 						<Button style={buttonStyle} type="submit" color="primary" variant="contained" fullWidth>
// 							Submit
// 						</Button>
// 					</form>
// 				</Grid>
// 				<Typography>
// 					<Link to="/register">Signup</Link>
// 				</Typography>
// 			</Paper>
// 		</Grid>
// 	);
// }
var react_2 = require("@chakra-ui/react");
var fa_1 = require("react-icons/fa");
var CFaUserAlt = react_2.chakra(fa_1.FaUserAlt);
var CFaLock = react_2.chakra(fa_1.FaLock);
function Login() {
    var _a = react_1.useState(''), email = _a[0], setEmail = _a[1];
    var _b = react_1.useState(''), password = _b[0], setPassword = _b[1];
    var navigate = react_router_dom_1.useNavigate();
    var _c = react_1.useState(false), showPassword = _c[0], setShowPassword = _c[1];
    var toast = react_2.useToast();
    var handleShowClick = function () { return setShowPassword(!showPassword); };
    function handleAlert() {
        toast({
            title: 'Log out',
            description: 'Logged Out Successfully!',
            status: 'success',
            duration: 2000,
            isClosable: true
        });
    }
    function check(e) {
        return __awaiter(this, void 0, void 0, function () {
            var payload, res;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        e.preventDefault();
                        payload = { email: email, password: password };
                        return [4 /*yield*/, utility_1.apiCall('/login', { email: email, password: password })];
                    case 1:
                        res = _a.sent();
                        if (res.status === 'ok') {
                            localStorage.setItem('token', res.data); //changing this to refresh token
                            console.log(res.data, 'in front res.data');
                            // <Alert status="success">
                            // 	<AlertIcon />
                            // 	Data uploaded to the server. Fire on!
                            // </Alert>;
                            navigate('/', { replace: true }); //the user is pushed to the feeed page once they are logged in
                        }
                        else {
                            alert(res.error); //error property if login not successful
                        }
                        return [2 /*return*/];
                }
            });
        });
    }
    return (React.createElement(react_2.Flex, { flexDirection: "column", width: "100wh", height: "100vh", backgroundColor: "gray.200", justifyContent: "center", alignItems: "center" },
        React.createElement(react_2.Stack, { flexDir: "column", mb: "2", justifyContent: "center", alignItems: "center" },
            React.createElement(react_2.Avatar, { bg: "grey.200" }),
            React.createElement(react_2.Heading, { color: "blue.300" }, "Welcome"),
            React.createElement(react_2.Box, { minW: { base: '90%', md: '468px' } },
                React.createElement("form", { onSubmit: check },
                    React.createElement(react_2.Stack, { spacing: 4, p: "1rem", backgroundColor: "whiteAlpha.900", boxShadow: "md" },
                        React.createElement(react_2.FormControl, null,
                            React.createElement(react_2.InputGroup, null,
                                React.createElement(react_2.InputLeftElement, { pointerEvents: "none", children: React.createElement(CFaUserAlt, { color: "gray.300" }) }),
                                React.createElement(react_2.Input, { type: "email", value: email, placeholder: "Enter Email", required: true, onChange: function (e) { return setEmail(e.target.value); } }))),
                        React.createElement(react_2.FormControl, null,
                            React.createElement(react_2.InputGroup, null,
                                React.createElement(react_2.InputLeftElement, { pointerEvents: "none", color: "gray.300", children: React.createElement(CFaLock, { color: "gray.300" }) }),
                                React.createElement(react_2.Input, { value: password, required: true, onChange: function (e) { return setPassword(e.target.value); }, type: showPassword ? 'text' : 'password', placeholder: "Password" }),
                                React.createElement(react_2.InputRightElement, { width: "4.5rem" },
                                    React.createElement(react_2.Button, { h: "1.75rem", size: "sm", onClick: handleShowClick }, showPassword ? 'Hide' : 'Show'))),
                            React.createElement(react_2.FormHelperText, { textAlign: "right" },
                                React.createElement(react_2.Link, null, "forgot password?"))),
                        React.createElement(react_2.Button, { borderRadius: 0, type: "submit", variant: "solid", className: "buttonC", width: "full", onClick: function () {
                                toast({
                                    title: 'Log in',
                                    description: 'Logged in Successfully!',
                                    status: 'success',
                                    duration: 2000,
                                    isClosable: true
                                });
                            } }, "Login"))))),
        React.createElement(react_2.Box, null,
            "New to us?",
            ' ',
            React.createElement(react_2.Link, { color: "blue.500", href: "/register" }, "Sign Up"))));
}
exports["default"] = Login;
