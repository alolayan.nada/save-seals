import { useState } from 'react';
import { apiCall } from '../../utility';
import { useNavigate } from 'react-router-dom';

import {
	Flex,
	Heading,
	Input,
	Button,
	InputGroup,
	Stack,
	InputLeftElement,
	chakra,
	Box,
	Link,
	Avatar,
	FormControl,
	InputRightElement,
	useToast,
} from '@chakra-ui/react';
import { FaUserAlt, FaLock } from 'react-icons/fa';

const CFaUserAlt = chakra(FaUserAlt);
const CFaLock = chakra(FaLock);

export default function Login() {
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const navigate = useNavigate();
	const [showPassword, setShowPassword] = useState(false);
	const toast = useToast();

	const handleShowClick = () => setShowPassword(!showPassword);

	async function check(e: any) {
		e.preventDefault();

		const res = await apiCall('/login', { email, password });
		if (res.status === 'ok') {
			toast({
				title: 'Log in',
				description: 'Logged in Successfully!',
				status: 'success',
				duration: 2000,
				isClosable: true,
			});
			localStorage.setItem('token', res.data); //changing this to refresh token
			console.log(res.data, 'in front res.data');

			navigate('/', { replace: true }); //the user is pushed to the feeed page once they are logged in
		} else {
			alert(res.error); //error property if login not successful
		}
	}

	return (
		<Flex
			flexDirection="column"
			width="100wh"
			height="100vh"
			backgroundColor="gray.200"
			justifyContent="center"
			alignItems="center"
		>
			<Stack flexDir="column" mb="2" justifyContent="center" alignItems="center">
				<Avatar bg="grey.200" />
				<Heading color="blue.300">Welcome</Heading>
				<Box minW={{ base: '90%', md: '468px' }}>
					<form onSubmit={check}>
						<Stack spacing={4} p="1rem" backgroundColor="whiteAlpha.900" boxShadow="md">
							<FormControl>
								<InputGroup>
									<InputLeftElement
										pointerEvents="none"
										children={<CFaUserAlt color="gray.300" />}
									/>
									<Input
										type="email"
										value={email}
										placeholder="Enter Email"
										required
										onChange={(e) => setEmail(e.target.value)}
									/>
								</InputGroup>
							</FormControl>
							<FormControl>
								<InputGroup>
									<InputLeftElement
										pointerEvents="none"
										color="gray.300"
										children={<CFaLock color="gray.300" />}
									/>
									<Input
										value={password}
										required
										onChange={(e) => setPassword(e.target.value)}
										type={showPassword ? 'text' : 'password'}
										placeholder="Password"
									/>
									<InputRightElement width="4.5rem">
										<Button h="1.75rem" size="sm" onClick={handleShowClick}>
											{showPassword ? 'Hide' : 'Show'}
										</Button>
									</InputRightElement>
								</InputGroup>
							</FormControl>
							<Button
								borderRadius={0}
								type="submit"
								variant="solid"
								className="buttonC"
								width="full"
								onClick={() => {}}
							>
								Login
							</Button>
						</Stack>
					</form>
				</Box>
			</Stack>
			<Box>
				New to us?{' '}
				<Link color="blue.500" href="/register">
					Sign Up
				</Link>
			</Box>
			<Box>
				<Link color="blue.500" href="\">
					Home
				</Link>
			</Box>
		</Flex>
	);
}
