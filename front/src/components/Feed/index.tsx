import { useEffect, useState } from 'react';
import Post from '../Post';
import { apiCallGet, apiCall } from '../../utility';
import { CircularProgress, Center, Flex, Text } from '@chakra-ui/react';
import Posts from '../Posts';

require('dotenv').config();

type post = {
	email: string;
	likes: number;
	title: string;
	description: string;
	image: String;
	username: String;
	messages: Message;
};

type Message = {
	user: string;
	postId: string;
	message: string;
	intent: 'chat';
	//add date
};

export default function Feed(props: any) {
	const [posts, setPosts] = useState([{}]);
	const [email, setEmail] = useState('');

	useEffect(() => {
		const getPosts = async () => {
			const postsfromServer = await fetchData();
			setPosts(postsfromServer);
			console.log('from server!!', postsfromServer);
			if (localStorage.token) {
				const userEmail = await getTokenInfo();
				setEmail(userEmail);
			}
		};
		getPosts();
		console.log('useeffect', posts);
	}, []);

	const fetchData = async () => {
		const r = await apiCallGet('/getAllPosts', [{}]);
		return r.result;
	};

	async function getTokenInfo() {
		let token = localStorage.token;
		const tokenParts = token.split('.');
		const encodedPayload = tokenParts[1];
		const rawPayload = atob(encodedPayload);
		const user = JSON.parse(rawPayload);
		return user.email;
	}

	const addPost = async (post: post) => {
		//get username from
		const newtask = {
			email: email,
			like: post.likes,
			image: post.image,
			title: post.title,
			messages: post.messages,
			description: post.description,
			date: new Date('Thu Dec 29 2011 20:14:56 GMT-0600 (CST)'),
		};
		console.log(newtask.date);

		const res = await apiCall('/api/post', newtask);
		setPosts([res.result, ...posts]);
		console.log(res.result, 'responded to add');
	};

	return (
		<div>
			<div>
				{localStorage.token ? (
					<Posts addPost={addPost}></Posts>
				) : (
					<Flex flex={1} bg="gray.100" justify="center" align="center" margin={10} padding={10}>
						<Text fontSize="3xl" opacity="20%">
							Please log in to post
						</Text>
					</Flex>
				)}
				{posts.length > 1 ? (
					<Post posts={posts} />
				) : (
					<Center p={10}>
						<CircularProgress isIndeterminate color="green.300" />
					</Center>
				)}
			</div>
		</div>
	);
}
