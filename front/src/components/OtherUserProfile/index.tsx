import Post from '../Post';
import Followers from '../Followers';
//import './Profile.css';
import { useEffect, useState } from 'react';

import {
	Stack,
	Box,
	useColorModeValue,
	Center,
	Heading,
	Image,
	Button,
	Text,
	Flex,
	Avatar,
} from '@chakra-ui/react';

require('dotenv').config();

export default function Profile(props: any) {
	console.log(props, 'this is props');
	const [followers, setUserFollower] = useState([{}]);
	const [posts, setPosts] = useState([{}]);
	const [showPost, setShowPost] = useState(false);

	return (
		<div>
			<Center py={6}>
				<Stack
					borderWidth="1px"
					borderRadius="lg"
					w={{ sm: '100%', md: '540px' }}
					height={{ sm: '476px', md: '20rem' }}
					direction={{ base: 'column', md: 'row' }}
					bg={useColorModeValue('white', 'gray.900')}
					boxShadow={'2xl'}
					padding={4}
				>
					<Stack flex={1} flexDirection="column" alignItems="center" justifyContent="center">
						<Avatar size="2xl" src={props.Followe.userInformation.data.avatar} />
						{/* <Image objectFit="contain" boxSize="100%" src={props.userInformation.data.avatar} /> */}
					</Stack>

					<Stack flex={1} flexDirection="column" alignItems="center" justifyContent="center">
						<Heading fontSize={'2xl'} fontFamily={'body'} alignItems="center">
							{props.Followe.userInformation.data.username}
						</Heading>

						<Text
							textAlign={'center'}
							color={useColorModeValue('gray.700', 'gray.400')}
							px={3}
						></Text>
						<Stack align={'center'} justify={'center'} direction={'row'} mt={6}></Stack>
						<Stack justifyContent="left">
							<Button as={Button} colorScheme="gray" onClick={() => setShowPost(!showPost)}>
								<Text>Posts: {posts.length}</Text>
							</Button>

							<Box fontSize={'md'}>
								<Text>
									{followers.length < 1 ? (
										<Button as={Button} colorScheme="gray" onClick={() => setShowPost(!showPost)}>
											<Text>Friends: 0</Text>
										</Button>
									) : (
										<Followers userFollower={followers}></Followers>
									)}
								</Text>
							</Box>
						</Stack>
					</Stack>
				</Stack>
			</Center>
			{showPost ? <Post posts={posts}></Post> : ''}
		</div>
	);
}
