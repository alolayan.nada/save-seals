import { Link } from 'react-router-dom';
import { apiCall } from '../../utility';
import { useNavigate } from 'react-router-dom';
import { useState } from 'react';
import ImageUpload from '../ImageUpload';

import './Register.css';

import {
	Stack,
	InputGroup,
	InputLeftElement,
	InputRightElement,
	Input,
	FormControl,
	Box,
	Heading,
	Avatar,
	Button,
	Flex,
	chakra,
	useToast,
} from '@chakra-ui/react';
import { FaUserAlt, FaLock } from 'react-icons/fa';

const CFaUserAlt = chakra(FaUserAlt);
const CFaLock = chakra(FaLock);

export default function Register() {
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [username, setUsername] = useState('');
	const [avatar, setAvatar] = useState(
		'https://firebasestorage.googleapis.com/v0/b/seals-2f4b0.appspot.com/o/images%2Fdownload%20(1).jpeg?alt=media&token=c5848127-503b-4d19-8f9b-8d339174df06'
	);
	const [showPassword, setShowPassword] = useState(false);

	const handleShowClick = () => setShowPassword(!showPassword);
	const toast = useToast();

	const navigate = useNavigate();

	const handleavatar = async (avatar: any) => {
		console.log(avatar, 'avatar');
		setAvatar(avatar);
	};

	async function register(e: any) {
		e.preventDefault();

		const res = await apiCall('/register', {
			email: email,
			password: password,
			username: username,
			avatar: avatar,
		});
		if (res.status === 'ok') {
			console.log(res.status);
			toast({
				title: 'Register',
				description: 'registered Successfully, login to continue!',
				status: 'success',
				duration: 7000,
				isClosable: true,
			});
			navigate('/login', { replace: true });
		} else {
			alert(res.error);
		}
	}
	return (
		<Flex
			flexDirection="column"
			width="100wh"
			height="100vh"
			backgroundColor="gray.200"
			justifyContent="center"
			alignItems="center"
		>
			<Stack flexDir="column" mb="2" justifyContent="center" alignItems="center">
				<Avatar src={avatar} />

				<Heading color="blue.300">Welcome</Heading>
				<Box minW={{ base: '90%', md: '468px' }}>
					<form onSubmit={register}>
						<Stack spacing={4} p="1rem" backgroundColor="whiteAlpha.900" boxShadow="md">
							<FormControl>
								<InputGroup>
									<InputLeftElement
										pointerEvents="none"
										children={<CFaUserAlt color="gray.300" />}
									/>

									<Input
										type="name"
										value={username}
										placeholder="Enter Username"
										required
										onChange={(e) => {
											setUsername(e.target.value);
										}}
									/>
								</InputGroup>
							</FormControl>
							<FormControl>
								<InputGroup>
									<InputLeftElement
										pointerEvents="none"
										children={<CFaUserAlt color="gray.300" />}
									/>

									<Input
										type="email"
										value={email}
										placeholder="Enter Email"
										required
										onChange={(e) => {
											setEmail(e.target.value);
										}}
									/>
								</InputGroup>
							</FormControl>
							<FormControl>
								<InputGroup>
									<InputLeftElement
										pointerEvents="none"
										color="gray.300"
										children={<CFaLock color="gray.300" />}
									/>
									<Input
										value={password}
										required
										onChange={(e) => {
											setPassword(e.target.value);
										}}
										type={showPassword ? 'text' : 'password'}
										placeholder="Password"
									/>
									<InputRightElement width="4.5rem">
										<Button h="1.75rem" size="sm" onClick={handleShowClick}>
											{showPassword ? 'Hide' : 'Show'}
										</Button>
									</InputRightElement>
								</InputGroup>
							</FormControl>

							<label>Upload Avatar</label>
							<FormControl>
								<InputGroup>
									<ImageUpload geturl={handleavatar} />
								</InputGroup>
							</FormControl>
							<Button
								borderRadius={0}
								type="submit"
								variant="solid"
								className="buttonC"
								width="full"
								onClick={() => {}}
							>
								Register
							</Button>
						</Stack>
					</form>
				</Box>
			</Stack>
			<Box>
				Already a member?{' '}
				<Link color="blue.500" to="/login">
					<u>Log in</u>
				</Link>
			</Box>
			<Box>
				<Link color="blue.500" to="/">
					<u>Home</u>
				</Link>
			</Box>
		</Flex>
	);
}
