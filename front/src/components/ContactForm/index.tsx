import { useState, useRef } from 'react';
import emailjs from 'emailjs-com';

import {
	Stack,
	InputGroup,
	Input,
	FormControl,
	Box,
	Center,
	useColorModeValue,
	Textarea,
	Image,
	Button,
} from '@chakra-ui/react';

export default function ContactForm() {
	require('dotenv').config();
	const form: any = useRef();
	const [status, setStatus] = useState('Submit');

	const handleSubmit = async (e: any) => {
		e.preventDefault();
		console.log(form.current);
		//@ts-ignore
		emailjs
			.sendForm(
				//@ts-ignore
				process.env.EMAILJS_SERVICE_ID,
				process.env.EMAILJS_TEMPLATE_ID,
				form.current,
				process.env.EMAILJS_USERID
			)
			.then(
				(result) => {
					setStatus('Thanks for contacting us!');
					console.log(result.text);
				},
				(error) => {
					console.log(error.text);
				}
			);
	};
	return (
		<div>
			<Center py={20}>
				<Box
					maxW={'600px'}
					w={'full'}
					bg={useColorModeValue('white', 'gray.900')}
					boxShadow={'2xl'}
					rounded={'md'}
					p={6}
					overflow={'hidden'}
				>
					<form ref={form} onSubmit={handleSubmit}>
						<Stack spacing={2}>
							<label htmlFor="name">Name</label>
							<FormControl isRequired>
								<InputGroup>
									<Input name="name" type="title" placeholder="Title" aria-label="Title" />
								</InputGroup>
							</FormControl>
							<label htmlFor="email">Email</label>
							<FormControl isRequired>
								<InputGroup>
									<Input name="email" type="email" placeholder="Email" aria-label="Email" />
								</InputGroup>
							</FormControl>
							<FormControl>
								<label htmlFor="message">Message:</label>
								<InputGroup>
									<Textarea
										name="message"
										placeholder="description"
										aria-label="description"
										rows={10}
										id="message"
									/>
								</InputGroup>
							</FormControl>
						</Stack>
						<Button borderRadius={0} type="submit" variant="solid" className="buttonC" width="full">
							Submit
						</Button>
					</form>
				</Box>
			</Center>
		</div>
	);
}
