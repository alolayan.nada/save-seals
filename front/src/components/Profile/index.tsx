import Post from '../Post';
import Followers from '../Followers';
import './Profile.css';
import { useEffect, useState } from 'react';

import {
	Stack,
	Box,
	useColorModeValue,
	Center,
	Heading,
	Image,
	Button,
	Text,
	Flex,
	Avatar,
} from '@chakra-ui/react';

require('dotenv').config();

export default function Profile(props: any) {
	const [followers, setUserFollower] = useState([{}]);
	const [posts, setPosts] = useState([{}]);
	const [showPost, setShowPost] = useState(false);
	const [isClicked, setIsclicked] = useState(false);
	//@ts-ignore
	const [clickedUserpost, setClickedUserpost] = useState();

	useEffect(() => {
		if (localStorage.token) {
			if (isClicked) {
				console.log('iss clickedd@!!!');
				console.log(clickedUserpost);
			} else {
				console.log('notclcicked');
				fetchOwnPosts();
				ownFollower('1');
			}
		}
	}, [isClicked]);

	function isClickedfunc(user: any) {
		//@ts-ignore
		let temp: any = [];
		temp.push(user.post);
		console.log(temp, 'temp');
		//@ts-ignore
		setClickedUserpost(temp);
		console.log(clickedUserpost);

		setIsclicked(true);
		setShowPost(!showPost);
	}
	//

	async function fetchOwnPosts() {
		fetch('/api/ownposts', {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
				'x-access-token': localStorage.getItem('token') || '', //check if we have jtw token which is created after each log-in
				token: localStorage.token || '',
				Accept: 'application/json',
			},
		})
			.then((res) => res.json())
			.then((data) => {
				data.result ? setPosts(data.result) : setPosts([{}]);
			})
			.catch((error) => console.log(error));
	}
	async function ownFollower(id: any) {
		const url = '/api/followers/';
		const path = id;
		fetch(`${url}${path}`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'x-access-token': localStorage.getItem('token') || '', //check if we have jtw token which is created after each log-in
				token: localStorage.token || '',
				Accept: 'application/json',
			},
		})
			.then((res) => res.json())
			.then((data) => {
				console.log(data, 'followers');
				data ? setUserFollower(data.result) : setUserFollower([{}]);
			})
			.catch((error) => console.log(error));
	}

	return (
		<div>
			<Center py={6}>
				<Stack
					borderWidth="1px"
					borderRadius="lg"
					w={{ sm: '100%', md: '540px' }}
					height={{ sm: '476px', md: '20rem' }}
					direction={{ base: 'column', md: 'row' }}
					bg={useColorModeValue('white', 'gray.900')}
					boxShadow={'2xl'}
					padding={4}
				>
					<Stack flex={1} flexDirection="column" alignItems="center" justifyContent="center">
						<Avatar size="2xl" src={props.userInformation.data.avatar} />
						{/* <Image objectFit="contain" boxSize="100%" src={props.userInformation.data.avatar} /> */}
					</Stack>

					<Stack flex={1} flexDirection="column" alignItems="center" justifyContent="center">
						<Heading fontSize={'2xl'} fontFamily={'body'} alignItems="center">
							{props.userInformation.data.username}
						</Heading>

						<Text
							textAlign={'center'}
							color={useColorModeValue('gray.700', 'gray.400')}
							px={3}
						></Text>
						<Stack align={'center'} justify={'center'} direction={'row'} mt={6}></Stack>
						<Stack justifyContent="left">
							<Button
								as={Button}
								colorScheme="gray"
								onClick={() => {
									setShowPost(!showPost);
									setIsclicked(false);
								}}
							>
								<Text>My posts: {posts.length}</Text>
							</Button>

							<Box fontSize={'md'}>
								<Text>
									{followers.length < 1 ? (
										<Button as={Button} colorScheme="gray" onClick={() => setShowPost(!showPost)}>
											<Text>Friends: 0</Text>
										</Button>
									) : (
										<Followers userFollower={followers} isClickedfunc={isClickedfunc}></Followers>
									)}
								</Text>
							</Box>
						</Stack>
					</Stack>
				</Stack>
			</Center>
			{showPost && !isClicked ? <Post posts={posts}></Post> : <Post posts={clickedUserpost}></Post>}
		</div>
	);
}

//userFollower={userFollower}
