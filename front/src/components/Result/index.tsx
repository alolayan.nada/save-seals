import { Paper, Typography, Card, CardContent } from '@material-ui/core';
const paperStyle = { padding: 20, height: '50vh', width: '150vh', margin: '20px auto' };

export default function SinglePost() {
	return (
		<Paper elevation={10} style={paperStyle}>
			<Card>
				<CardContent>
					<Typography variant="h5" component="div"></Typography>
				</CardContent>
			</Card>
		</Paper>
	);
}
