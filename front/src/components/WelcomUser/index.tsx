import { Avatar, AvatarBadge, HStack } from '@chakra-ui/react';
import './WelcomeUser.css';

const WelcomUser = (props: any) => {
	console.log(props);
	return (
		<div>
			<HStack>
				<p className="pn">Welcome {props.userInfo.data.username}</p>
				<Avatar src={props.userInfo.data.avatar}>
					<AvatarBadge boxSize="1.25em" bg="green.500" />
				</Avatar>
			</HStack>
		</div>
	);
};

export default WelcomUser;
