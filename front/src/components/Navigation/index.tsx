import { Link } from 'react-router-dom';
import { useRef } from 'react';

import { Button, useToast, HStack } from '@chakra-ui/react';

const Navigation = (props: any) => {
	const section = useRef(null);
	const toast = useToast();
	return (
		<div>
			{localStorage.token ? (
				//	<Flex w="100%" px="6" py="5" align="center" justify="space-between">
				<HStack as="nav" spacing="2">
					<Button
						backgroundColor="white"
						size="md"
						height="48px"
						width="150px"
						onClick={() => {
							props.Buttons.showComp('News');
						}}
					>
						{'About us'}
					</Button>

					<Button
						backgroundColor="white"
						size="md"
						height="48px"
						width="150px"
						onClick={() => props.Buttons.showComp('feed')}
					>
						{'Community'}
					</Button>

					<Button
						backgroundColor="white"
						size="md"
						height="48px"
						width="150px"
						onClick={() => props.Buttons.showComp('profile')}
					>
						{'Profile'}
					</Button>

					<Button
						backgroundColor="white"
						size="md"
						height="48px"
						width="150px"
						onClick={() => {
							props.Buttons.logOut();
							toast({
								title: 'Log out',
								description: 'Logged Out Successfully!',
								status: 'success',
								duration: 2000,
								isClosable: true,
							});
						}}
					>
						logout
					</Button>
					<Button
						backgroundColor="white"
						size="md"
						height="48px"
						width="150px"
						onClick={() => props.Buttons.showComp('contact')}
					>
						{'Contact us'}
					</Button>
				</HStack>
			) : (
				//</Flex>
				//	<Flex w="100%" px="6" py="5" align="center" justify="space-between">
				<HStack as="nav" spacing="2">
					<Button
						backgroundColor="white"
						size="md"
						height="48px"
						width="150px"
						onClick={() => props.Buttons.showComp('contact')}
					>
						{'Contact us'}
					</Button>
					<Button
						backgroundColor="white"
						size="md"
						height="48px"
						width="150px"
						onClick={() => props.Buttons.showComp('News')}
					>
						{'About us'}
					</Button>
					<Button
						backgroundColor="white"
						size="md"
						height="48px"
						width="150px"
						onClick={() => props.Buttons.showComp('feed')}
					>
						{'Community'}
					</Button>
					<Link to="/login">
						<Button backgroundColor="white" size="md" height="48px" width="150px">
							Login
						</Button>
					</Link>
					<Link to="/register">
						<Button backgroundColor="white" size="md" height="48px" width="150px">
							Register
						</Button>
					</Link>
				</HStack>
				//	</Flex>
			)}
		</div>
	);
};

export default Navigation;
