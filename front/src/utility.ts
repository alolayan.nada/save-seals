//created this simple fetch wrapper instead of axios
require('dotenv').config();

export const IS_DEVELOPMENT = window.location.hostname === 'localhost'; //if you write on terminal : location.hostname you get localhost
export const IS_PRODUCTION = !IS_DEVELOPMENT;

const API_URL = 'https://seals-help.herokuapp.com';
// || 'http://localhost:5000';

export async function apiCall(path: string, payload: { [key: string]: any }) {
	console.log(payload, 'this is payload');
	const res = await fetch(`${API_URL}${path}`, {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			'x-access-token': localStorage.getItem('token') || '', //check if we have jtw token which is created after each log-in
			token: localStorage.token || '',
		},
		body: JSON.stringify(payload),
	}).then((t) => t.json());

	return res;
}

export async function apiCallGet(path: string, payload: { [key: string]: any }) {
	console.log('here##', path);
	const res = await fetch(`${API_URL}${path}`, {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json',
			'x-access-token': localStorage.getItem('token') || '', //check if we have jtw token which is created after each log-in
			token: localStorage.token || '',
		},
	}).then((t) => t.json());
	console.log(res.result, 'apicall');
	return res;
}
