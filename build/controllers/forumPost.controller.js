"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.decreaseLike = exports.increaseLike = exports.getMessages = exports.getOwnPosts = exports.getAllPosts = exports.post = void 0;
const users_model_1 = __importDefault(require("../models/users.model"));
const posts_model_1 = __importDefault(require("../models/posts.model"));
//@ts-ignore
const post = async (req, res) => {
    const email = req.body.email;
    const image = req.body.image;
    const title = req.body.title;
    const description = req.body.description;
    const date = req.body.date;
    //get the user name from the email because the email is the key not the user name
    //const arr: String[] = uriPost.split(":");
    //const uri = `${arr[1]}/${arr[2]}`;
    //something to varify
    //@ts-ignore
    const postusername = await users_model_1.default.findOne({ email: email });
    console.log(postusername.username);
    try {
        const post = new posts_model_1.default({
            email: email,
            //@ts-ignore
            username: postusername.username,
            image: image,
            title: title,
            description: description,
            date: date,
        });
        //@ts-ignore
        const result = await post.save();
        //@ts-ignore
        const resultUser = await users_model_1.default.updateOne({ email: email }, {
            $push: {
                posts: result._id,
            },
        });
        res.json({ result });
    }
    catch (error) {
        console.log('Error:', error);
        res.json({ status: 'errorhhhhh' });
    }
};
exports.post = post;
const getAllPosts = async (req, res) => {
    const allPosts = await posts_model_1.default.find({}).sort({ _id: -1 }).exec();
    const result = [];
    for (const post of allPosts) {
        //@ts-ignore
        const check = await users_model_1.default.findOne({ email: post.email })
            .then((user) => {
            const obj = {
                //@ts-ignore
                username: post.username,
                //@ts-ignore
                email: post.email,
                //@ts-ignore
                description: post.description,
                //@ts-ignore
                image: post.image,
                avatar: user.avatar,
                //@ts-ignore
                title: post.title,
                //@ts-ignore
                date: post.date.toUTCString(),
                //@ts-ignore
                messages: post.messages,
                //@ts-ignore
                like: post.like,
            };
            result.push(obj);
        })
            .catch((error) => console.log(error));
    }
    res.json({ result });
};
exports.getAllPosts = getAllPosts;
const getOwnPosts = async (req, res) => {
    const userOwn = res.locals.user;
    const users = await users_model_1.default.find({ email: userOwn.email }) /// users with posts
        .populate({
        path: 'posts',
    })
        .exec();
    console.log(users, 'hghghgh');
    const result = [];
    for (const post of users[0].posts) {
        const obj = {
            username: post.username,
            email: post.email,
            description: post.description,
            image: post.image,
            avatar: users[0].avatar,
            title: post.title,
            date: post.date.toUTCString(),
            messages: post.messages,
            like: post.like,
        };
        console.log('here', users.avatar);
        result.push(obj);
    }
    // users.forEach((user: any) => {
    // 	if (user.posts.length > 0) {
    // 		const obj: userObj = {
    // 			username: user.username,
    // 			post: user.posts,
    // 		};
    // 		result.push(obj);
    // 	}
    // }); this is needed  if I want to get posts for more than one user
    console.log('result go t', result);
    res.json({ result });
};
exports.getOwnPosts = getOwnPosts;
const getMessages = async (req, res) => {
    const postId = req.params.postid;
    const results = await posts_model_1.default.findOne({ _id: postId })
        .populate({
        path: 'messages',
    })
        .exec();
    const messages = results.messages;
    const result = [];
    messages.forEach((message) => {
        const obj = {
            username: message.email,
            message: message.message,
        };
        result.push(obj);
    });
    res.json({ result });
};
exports.getMessages = getMessages;
const increaseLike = async (req, res) => {
    const postId = req.params.postid;
    const result = await posts_model_1.default.findOne({ _id: postId });
    const like = result.like + 1;
    const resultInsert = await posts_model_1.default.updateOne({ _id: postId }, {
        like: like,
    });
    if (resultInsert.ok === 1) {
        res.json({ status: 'ok', like: like });
    }
    else {
        res.json({ status: 'ng' });
    }
};
exports.increaseLike = increaseLike;
const decreaseLike = async (req, res) => {
    const postId = req.params.postid;
    const result = await posts_model_1.default.findOne({ _id: postId });
    const like = result.like === 0 ? 0 : result.like - 1;
    const resultInsert = await posts_model_1.default.updateOne({ _id: postId }, {
        like: like,
    });
    if (resultInsert.ok === 1) {
        res.json({ status: 'ok', like: like });
    }
    else {
        res.json({ status: 'ng' });
    }
};
exports.decreaseLike = decreaseLike;
