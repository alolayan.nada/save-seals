export declare const post: (req: any, res: any) => Promise<void>;
export declare const getAllPosts: (req: any, res: any) => Promise<void>;
export declare const getOwnPosts: (req: any, res: any) => Promise<void>;
export declare const getMessages: (req: any, res: any) => Promise<void>;
export declare const increaseLike: (req: any, res: any) => Promise<void>;
export declare const decreaseLike: (req: any, res: any) => Promise<void>;
