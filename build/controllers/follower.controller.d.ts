export declare const addFollower: (req: any, res: any) => Promise<void>;
export declare const getOwnFollower: (req: any, res: any) => Promise<void>;
