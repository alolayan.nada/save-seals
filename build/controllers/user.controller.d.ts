export declare const register: (req: any, res: any) => Promise<any>;
export declare const getIdfromEmail: (req: any, res: any) => Promise<any>;
export declare const getUserInfo: (req: any, res: any) => Promise<any>;
export declare const login: (req: any, res: any) => Promise<any>;
export declare const getUsers: (req: any, res: any) => Promise<void>;
export declare const getOtherUser: (req: any, res: any) => Promise<void>;
