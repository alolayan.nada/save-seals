"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getOwnFollower = exports.addFollower = void 0;
const followers_model_1 = __importDefault(require("../models/followers.model"));
const users_model_1 = __importDefault(require("../models/users.model"));
const addFollower = async (req, res) => {
    const userOwn = res.locals.user;
    const followUserId = req.params.userid; //what is the follower id?
    const newUser = userOwn.followers.toString().replace(/ObjectId\("(.*)"\)/, '$1');
    const result = await followers_model_1.default.updateOne({ _id: newUser }, {
        $push: {
            following: followUserId, //who am I following
        },
    });
    if (result.ok === 1) {
        res.json({ status: 'ok' });
    }
    else {
        res.json({ status: 'not good' });
    }
};
exports.addFollower = addFollower;
//show all people that I follow
const getOwnFollower = async (req, res) => {
    const userOwn = res.locals.user;
    // const followers = await Follower.findOne({ _id: userOwn.followers }).then((result) => {
    // 	return result;
    // });
    const followers = await followers_model_1.default.findOne({ _id: userOwn.followers }).then((result) => {
        const arr = [];
        result.following.map((item) => {
            if (!arr.includes(item.toString().replace(/ObjectId\("(.*)"\)/, '$1'))) {
                arr.push(item.toString().replace(/ObjectId\("(.*)"\)/, '$1'));
            }
        });
        return arr;
        // .filter((v, i, a) => a.indexOf(v) === i);
    });
    console.log(followers);
    // thi is actually following not followers for the user
    const result = [];
    for (const follower of followers) {
        const user = await users_model_1.default.findOne({ _id: follower })
            .select('_id username posts avatar')
            .populate({
            path: 'posts',
        })
            .exec();
        result.push(user);
    }
    const resultObj = [];
    result.forEach((data) => {
        const obj = {
            userid: data._id,
            username: data.username,
            post: data.posts[data.posts.length - 1],
        };
        resultObj.push(obj);
    });
    res.json({ result: resultObj });
};
exports.getOwnFollower = getOwnFollower;
//***/git log
