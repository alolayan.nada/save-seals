"use strict";
//all activities for the user model
//*****  followers changed to following */
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getOtherUser = exports.getUsers = exports.login = exports.getUserInfo = exports.getIdfromEmail = exports.register = void 0;
const users_model_1 = __importDefault(require("../models/users.model"));
const followers_model_1 = __importDefault(require("../models/followers.model"));
const crypto_1 = require("../crypto");
const jwt = __importStar(require("jsonwebtoken"));
const JWT_SECRET_TOKEN = process.env.JWT_SECRET_TOKEN;
async function checkIfUserExists(email) {
    return users_model_1.default.findOne({ email: email })
        .then((user) => (user ? true : false))
        .catch(() => false);
}
const register = async (req, res) => {
    console.log(req.body);
    //we need to use bcrypt to hash the password
    const { email, password, username, avatar } = req.body;
    if (!email || !password || !username || !avatar) {
        return res.json({ status: 'error', error: 'Invalid email/password/username' });
    }
    const userExist = await checkIfUserExists(email);
    if (userExist) {
        return res.json({ status: 'error', error: 'Email is duplicated' });
    }
    try {
        //@ts-ignore
        const follower = await new followers_model_1.default({ followers: [] }).save();
        const passwordHash = (0, crypto_1.encrypto)(password);
        const user = new users_model_1.default({
            email,
            password: passwordHash,
            username,
            avatar,
            followers: follower._id,
        });
        await user.save();
        return res.json({ status: 'ok' });
    }
    catch (error) {
        console.log('Error', error);
        res.json({ status: 'error', error: 'error_' });
    }
    res.json({ status: 'New user saved' });
};
exports.register = register;
const getIdfromEmail = async (req, res) => {
    console.log('from the email to id');
    const { email } = req.body;
    console.log('from the email to id', email);
    const user = await users_model_1.default.findOne({ email });
    console.log(user._id);
    return res.json(user._id);
};
exports.getIdfromEmail = getIdfromEmail;
const getUserInfo = async (req, res) => {
    console.log('user info');
    const userOwn = res.locals.user;
    return res.json(userOwn);
};
exports.getUserInfo = getUserInfo;
const login = async (req, res) => {
    console.log('here');
    console.log(req.body, 'req.body');
    const { email, password } = req.body;
    const passwordHash = (0, crypto_1.encrypto)(password);
    console.log(passwordHash);
    const user = await users_model_1.default.findOne({ email, password: passwordHash }).lean(); //match the email and password and the record for that user . "Lean" keyword  will remove out all the meta data coming with the mongo object and get us a plain json object
    console.log(user, 'this is user');
    if (!user) {
        return res.json({ status: 'error', error: 'Wrong username or password' });
    }
    const payload = jwt.sign({ email, username: user.username }, JWT_SECRET_TOKEN); //payload of the user, this will SIGN the email with the token.
    //Note: the jwt_secret_token I wrote needs more randomness
    console.log(payload, 'payload in usercontroller');
    return res.json({ status: 'ok', data: payload });
};
exports.login = login;
//show users only if they have posts
const getUsers = async (req, res) => {
    const userOwn = res.locals.user;
    const users = await users_model_1.default.find({ email: { $ne: userOwn.email } })
        .populate({
        path: 'posts',
    })
        .exec();
    const result = [];
    users.forEach((user) => {
        if (user.posts.length > 0) {
            const obj = {
                username: user.username,
                userid: user._id,
                avatar: user.avatar,
                post: user.posts[user.posts.length - 1],
            };
            result.push(obj);
        }
    });
    res.json({ result });
};
exports.getUsers = getUsers;
//get the user info and show if they follow me or not
const getOtherUser = async (req, res) => {
    const userOwn = res.locals.user;
    const userId = req.params.userid;
    const result = await users_model_1.default.findOne({ _id: userId })
        .populate({
        path: 'posts',
    })
        .exec();
    const myFollow = await followers_model_1.default.findOne({ _id: userOwn.followers });
    const resultObj = {
        id: result._id,
        username: result.username,
        posts: result.posts,
        avatar: result.avatar,
        follow: myFollow.followers.includes(userId) ? true : false, //chek this for the tuble name followers?
    };
    res.json({ result: resultObj });
};
exports.getOtherUser = getOtherUser;
// //req from front end
// router.route('/new').post((req, res) => {
// 	const newUser = new User({
// 		username: req.body.username || {},
// 		email: req.body.email,
// 	});
// 	newUser
// 		.save()
// 		.then((user) => res.json(user))
// 		.catch((err) => res.status(400).json('Error! ' + err));
// });
// router.route('/').get((req, res) => {
// 	// using .find() without a parameter will match on all user instances
// 	User.find()
// 		.then((allUsers) => res.json(allUsers))
// 		.catch((err) => res.status(400).json('Error! ' + err));
// });
// router.route('/delete/:id').delete((req, res) => {
// 	User.deleteOne({ _id: req.params.id })
// 		.then((success) => res.json('Success! User deleted.'))
// 		.catch((err) => res.status(400).json('Error! ' + err));
// });
// router.route('/update/:id').put((req, res) => {
// 	User.findByIdAndUpdate(req.params.id, req.body)
// 		.then((user) => res.json('Success! User updated.'))
// 		.catch((err) => res.status(400).json('Error! ' + err));
// });
// module.exports = router;
