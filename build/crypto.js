"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.encrypto = void 0;
require('dotenv').config();
const crypto = require('crypto');
//var randomstring = require('randomstring');
const key = process.env.CRYPTO_KEY;
// const IV_LENGTH = 16;
// let iv = crypto.randomBytes(IV_LENGTH);
// // randomstring.generate({
// // 	length: 48,
// // 	charset: 'hex',
// // })
const encrypto = (password) => {
    const cipher = crypto.createCipher('aes192', key);
    cipher.update(password, 'utf8', 'hex');
    const cipheredText = cipher.final('hex');
    return cipheredText;
};
exports.encrypto = encrypto;
