"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.test = exports.verifyJWT = void 0;
require('dotenv').config();
const jsonwebtoken = require('jsonwebtoken');
const users_model_1 = __importDefault(require("./models/users.model"));
const JWT_SECRET_TOKEN = process.env.JWT_SECRET_TOKEN;
const verifyJWT = (req, res, next) => {
    let token = req.headers.token;
    if (token) {
        jsonwebtoken.verify(token, JWT_SECRET_TOKEN, (errors, payload) => {
            console.log(payload, 'this is content of payload');
            if (payload) {
                users_model_1.default.findOne({ email: payload.email }).then((user) => {
                    res.locals.user = user;
                    console.log('verfified!!');
                    next();
                });
            }
            else {
                res.status(401).json({
                    error: true,
                    message: 'cannnot verify API token',
                });
            }
        });
    }
    else {
        res.status(400).json({
            error: true,
            message: 'Provide Token',
        });
    }
};
exports.verifyJWT = verifyJWT;
//example: how to use JWT
const test = (req, res) => {
    //if JWT is autheticated, you can get user information from "res.locals.user"
    const user = res.locals.user;
    res.json({
        user,
    });
};
exports.test = test;
