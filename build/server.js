"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const cors_1 = __importDefault(require("cors"));
const mongoose_1 = __importDefault(require("mongoose"));
const path_1 = __importDefault(require("path"));
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config();
//export const ATLAS_CONNECTION = process.env.ATLAS_CONNECTION!;
const verifyJWT_1 = require("./verifyJWT");
const user_controller_1 = require("./controllers/user.controller");
const forumPost_controller_1 = require("./controllers/forumPost.controller");
const follower_controller_1 = require("./controllers/follower.controller");
const app = (0, express_1.default)();
app.use(express_1.default.json()); // To parse the incoming requests with JSON payloads
app.use((0, cors_1.default)());
app.post('/register', user_controller_1.register);
app.post('/login', user_controller_1.login);
//verifyJWT middleware.
//when endpoint is /api/*, always checking JWT.
app.use('/api', verifyJWT_1.verifyJWT);
//exapmle of how to use JWT
//test is a function -> check src/verifyJWT
app.post('/api/test', verifyJWT_1.test);
//get users
app.get('/api/user', user_controller_1.getUsers);
//get another user data
app.get('/api/user/:userid', user_controller_1.getOtherUser);
app.post('/api/post', forumPost_controller_1.post);
app.get('/api/getUserInfo', user_controller_1.getUserInfo);
//get own post history
app.get('/api/ownposts', forumPost_controller_1.getOwnPosts);
app.get('/api/getAllPosts', forumPost_controller_1.getAllPosts);
//get messages on the post
app.get('/api/message/:postid', forumPost_controller_1.getMessages);
//update like's count -> +1
app.patch('/api/like/:postid', forumPost_controller_1.increaseLike);
//update like's count -> -1
app.patch('/api/dislike/:postid', forumPost_controller_1.decreaseLike);
//add an user to followers list
app.patch('/api/follow/:userid', follower_controller_1.addFollower);
//get followers list
app.get('/api/followers', follower_controller_1.getOwnFollower);
app.post('/api/idfromEmail', user_controller_1.getIdfromEmail);
//
mongoose_1.default
    .connect(process.env.ATLAS_CONNECTION, {})
    .then((res) => {
    console.log('Connected ');
})
    .catch((err) => {
    console.log(` error occured -`, err);
});
if (process.env.DB_ENVIRONMENT === 'production') {
    console.log('test@#@#@#xs');
    // Serve any static files
    app.use(express_1.default.static(path_1.default.join(__dirname, '../front/build')));
    // Handle React routing, return all requests to React app
    app.get('/*', function (req, res) {
        res.sendFile(path_1.default.join(__dirname, '../front', 'build', 'index.html'));
        //res.sendFile('index.html', { root: '../front-end/build/' });
    });
    const port = process.env.PORT || 4000;
    app.listen(port, () => {
        console.log(`🚀 Server is running on ${port}`);
    });
}
