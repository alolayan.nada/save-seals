"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const Schema = mongoose_1.default.Schema;
const posts_model_1 = __importDefault(require("./posts.model"));
const followers_model_1 = __importDefault(require("./followers.model"));
const UserModel = new mongoose_1.default.Schema({
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    username: { type: String, required: true },
    posts: [{ type: Schema.Types.ObjectId, ref: posts_model_1.default, required: false }],
    followers: { type: Schema.Types.ObjectId, ref: followers_model_1.default, required: true },
    avatar: {
        type: String,
        required: false,
    },
}, { collection: 'users' });
const model = mongoose_1.default.model('UserModel', UserModel);
exports.default = model;
