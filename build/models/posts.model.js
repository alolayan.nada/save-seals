"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const Schema = mongoose_1.default.Schema;
const messages_model_1 = __importDefault(require("./messages.model"));
const PostModel = new mongoose_1.default.Schema({
    //add user name
    email: { type: String, required: true },
    username: { type: String, required: true },
    like: { type: Number, required: false, default: 0 },
    image: {
        type: String,
        required: true,
    },
    // `title` is required and of type String
    title: {
        type: String,
        required: true,
    },
    // `link` is required and of type String
    // link: {
    // 	type: String,
    // 	required: true,
    // },
    description: {
        type: String,
        required: true,
    },
    date: { type: Date, required: false },
    messages: [{ type: Schema.Types.ObjectId, ref: messages_model_1.default, required: false }],
}, { collection: 'posts' });
const model = mongoose_1.default.model('PostModel', PostModel);
exports.default = model;
