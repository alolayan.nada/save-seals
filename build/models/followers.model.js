"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const Schema = mongoose_1.default.Schema;
const users_model_1 = __importDefault(require("./users.model"));
const FollowerModel = new mongoose_1.default.Schema({
    following: [{ type: Schema.Types.ObjectId, ref: users_model_1.default, required: false }],
}, { collection: 'followers' });
const model = mongoose_1.default.model('FollowerModel', FollowerModel);
exports.default = model;
