# Save Seals

This project is dedicated to raise awareness of the brutalities inflicted on seals by humans and to connect those who are interested together.

# Demo
<a href="https://seals-help.herokuapp.com/" target="_blank"><img src="https://i.ibb.co/9qxnvpR/Screen-Shot-2022-05-02-at-7-11-58.png" alt="Screen-Shot-2022-05-02-at-7-11-58" border="0" align="center"></a>
<p></p>

https://seals-help.herokuapp.com/ 

## Motivation

This project is dedicated to raise awareness of the brutalities inflicted on seals by humans and to connect those who are interested together.

## Features
- login
- logout
- register
- profile
- post topic
- add friends
- contact form


## Tech/framework used

- React for front End/ TypeScript, Node.js and Express.js for the backend
- Used chakra UI library for user interface
- MongoDb for database
- JWT for authentication
