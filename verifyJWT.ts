require('dotenv').config();
const jsonwebtoken = require('jsonwebtoken');
import User from './models/users.model';

const JWT_SECRET_TOKEN = process.env.JWT_SECRET_TOKEN;

export const verifyJWT = (req: any, res: any, next: any) => {
	let token = req.headers.token;

	if (token) {
		jsonwebtoken.verify(token, JWT_SECRET_TOKEN, (errors: any, payload: any) => {
			console.log(payload, 'this is content of payload');
			if (payload) {
				User.findOne({ email: payload.email }).then((user: any) => {
					res.locals.user = user;
					console.log('verfified!!');
					next();
				});
			} else {
				res.status(401).json({
					error: true,
					message: 'cannnot verify API token',
				});
			}
		});
	} else {
		res.status(400).json({
			error: true,
			message: 'Provide Token',
		});
	}
};

//example: how to use JWT
export const test = (req: any, res: any) => {
	//if JWT is autheticated, you can get user information from "res.locals.user"
	const user = res.locals.user;
	res.json({
		user,
	});
};
