import mongoose from 'mongoose';
const Schema = mongoose.Schema;
import Message from './messages.model';

const PostModel = new mongoose.Schema(
	{
		//add user name

		email: { type: String, required: true }, //identifier
		username: { type: String, required: true },
		like: { type: Number, required: false, default: 0 },
		image: {
			type: String,
			required: true,
		},

		// `title` is required and of type String
		title: {
			type: String,
			required: true,
		},
		// `link` is required and of type String
		// link: {
		// 	type: String,
		// 	required: true,
		// },
		description: {
			type: String,
			required: true,
		},
		date: { type: Date, required: false },
		messages: [{ type: Schema.Types.ObjectId, ref: Message, required: false }],
	},
	{ collection: 'posts' }
);

const model: any = mongoose.model('PostModel', PostModel);

export default model;
