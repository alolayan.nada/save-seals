import mongoose from 'mongoose';
const Schema = mongoose.Schema;
import User from './users.model';

const FollowerModel: any = new mongoose.Schema(
	{
		following: [{ type: Schema.Types.ObjectId, ref: User, required: false }],
	},
	{ collection: 'followers' }
);

const model: any = mongoose.model('FollowerModel', FollowerModel);

export default model;
