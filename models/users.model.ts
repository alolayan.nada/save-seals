import mongoose from 'mongoose';
const Schema = mongoose.Schema;
import Post from './posts.model';
import Follower from './followers.model';

const UserModel = new mongoose.Schema(
	{
		email: { type: String, required: true, unique: true },
		password: { type: String, required: true }, //I didnt use unique here because two people can have the same password
		username: { type: String, required: true },
		posts: [{ type: Schema.Types.ObjectId, ref: Post, required: false }],
		followers: { type: Schema.Types.ObjectId, ref: Follower, required: true }, //who ami folloing not followers
		avatar: {
			type: String,
			required: false,
		},
	},
	{ collection: 'users' }
);

const model: any = mongoose.model('UserModel', UserModel);

export default model;
