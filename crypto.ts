require('dotenv').config();
const crypto = require('crypto');
//var randomstring = require('randomstring');

const key = process.env.CRYPTO_KEY;
// const IV_LENGTH = 16;
// let iv = crypto.randomBytes(IV_LENGTH);

// // randomstring.generate({
// // 	length: 48,
// // 	charset: 'hex',
// // })

export const encrypto = (password: string) => {
	const cipher = crypto.createCipher('aes192', key);
	cipher.update(password, 'utf8', 'hex');
	const cipheredText = cipher.final('hex');
	return cipheredText;
};
