//all activities for the user model
//*****  followers changed to following */

import User from '../models/users.model';
import Follower from '../models/followers.model';
import { encrypto } from '../crypto';
import * as jwt from 'jsonwebtoken';

const JWT_SECRET_TOKEN: any = process.env.JWT_SECRET_TOKEN;

async function checkIfUserExists(email: any) {
	return User.findOne({ email: email })
		.then((user: any) => (user ? true : false))
		.catch(() => false);
}

export const register = async (req: any, res: any) => {
	console.log(req.body);
	//we need to use bcrypt to hash the password
	const { email, password, username, avatar } = req.body;

	if (!email || !password || !username || !avatar) {
		return res.json({ status: 'error', error: 'Invalid email/password/username' });
	}
	const userExist: any = await checkIfUserExists(email);

	if (userExist) {
		return res.json({ status: 'error', error: 'Email is duplicated' });
	}

	try {
		//@ts-ignore
		const follower: any = await new Follower({ followers: [] }).save();
		const passwordHash = encrypto(password);
		const user = new User({
			email,
			password: passwordHash,
			username,
			avatar,
			followers: follower._id,
		});
		await user.save();
		return res.json({ status: 'ok' });
	} catch (error: any) {
		console.log('Error', error);
		res.json({ status: 'error', error: 'error_' });
	}
	res.json({ status: 'New user saved' });
};

export const getIdfromEmail = async (req: any, res: any) => {
	console.log('from the email to id');
	const { email } = req.body;
	console.log('from the email to id', email);
	const user = await User.findOne({ email });
	console.log(user._id);
	return res.json(user._id);
};

export const getUserInfo = async (req: any, res: any) => {
	console.log('user info');
	const userOwn = res.locals.user;
	return res.json(userOwn);
};

export const login = async (req: any, res: any) => {
	console.log('here');
	console.log(req.body, 'req.body');
	const { email, password } = req.body;
	const passwordHash = encrypto(password);
	console.log(passwordHash);
	const user: any = await User.findOne({ email, password: passwordHash }).lean(); //match the email and password and the record for that user . "Lean" keyword  will remove out all the meta data coming with the mongo object and get us a plain json object
	console.log(user, 'this is user');
	if (!user) {
		return res.json({ status: 'error', error: 'Wrong username or password' });
	}
	const payload = jwt.sign({ email, username: user.username }, JWT_SECRET_TOKEN); //payload of the user, this will SIGN the email with the token.
	//Note: the jwt_secret_token I wrote needs more randomness
	console.log(payload, 'payload in usercontroller');
	return res.json({ status: 'ok', data: payload });
};

interface userObj {
	username: string;
	userid: string;
	avatar: string;
	post: any;
}

//show users only if they have posts
export const getUsers = async (req: any, res: any) => {
	const userOwn = res.locals.user;
	const users = await User.find({ email: { $ne: userOwn.email } })
		.populate({
			path: 'posts',
		})
		.exec();
	const result: userObj[] = [];
	users.forEach((user: any) => {
		if (user.posts.length > 0) {
			const obj: userObj = {
				username: user.username,
				userid: user._id,
				avatar: user.avatar,
				post: user.posts[user.posts.length - 1],
			};
			result.push(obj);
		}
	});
	res.json({ result });
};

interface resultObj {
	id: String;
	username: String;
	posts: any;
	avatar: string;
	follow: Boolean;
}

//get the user info and show if they follow me or not
export const getOtherUser = async (req: any, res: any) => {
	const userOwn = res.locals.user;
	const userId = req.params.userid;
	const result = await User.findOne({ _id: userId })
		.populate({
			path: 'posts',
		})
		.exec();
	const myFollow: any = await Follower.findOne({ _id: userOwn.followers });
	const resultObj: resultObj = {
		id: result._id,
		username: result.username,
		posts: result.posts,
		avatar: result.avatar,
		follow: myFollow.followers.includes(userId) ? true : false, //chek this for the tuble name followers?
	};
	res.json({ result: resultObj });
};

// //req from front end
// router.route('/new').post((req, res) => {
// 	const newUser = new User({
// 		username: req.body.username || {},
// 		email: req.body.email,
// 	});

// 	newUser
// 		.save()
// 		.then((user) => res.json(user))
// 		.catch((err) => res.status(400).json('Error! ' + err));
// });

// router.route('/').get((req, res) => {
// 	// using .find() without a parameter will match on all user instances
// 	User.find()
// 		.then((allUsers) => res.json(allUsers))
// 		.catch((err) => res.status(400).json('Error! ' + err));
// });

// router.route('/delete/:id').delete((req, res) => {
// 	User.deleteOne({ _id: req.params.id })
// 		.then((success) => res.json('Success! User deleted.'))
// 		.catch((err) => res.status(400).json('Error! ' + err));
// });

// router.route('/update/:id').put((req, res) => {
// 	User.findByIdAndUpdate(req.params.id, req.body)
// 		.then((user) => res.json('Success! User updated.'))
// 		.catch((err) => res.status(400).json('Error! ' + err));
// });

// module.exports = router;
