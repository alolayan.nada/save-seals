import Follower from '../models/followers.model';
import User from '../models/users.model';

export const addFollower = async (req: any, res: any) => {
	const userOwn = res.locals.user;
	const followUserId = req.params.userid; //what is the follower id?
	const newUser: any = userOwn.followers.toString().replace(/ObjectId\("(.*)"\)/, '$1');

	const result: any = await Follower.updateOne(
		{ _id: newUser },
		{
			$push: {
				following: followUserId, //who am I following
			},
		}
	);
	if (result.ok === 1) {
		res.json({ status: 'ok' });
	} else {
		res.json({ status: 'not good' });
	}
};

interface followerObj {
	userid: String;
	username: String;
	post: any;
}

//show all people that I follow
export const getOwnFollower = async (req: any, res: any) => {
	const followUserId = req.params.userid;
	const userOwn = res.locals.user;
	console.log(userOwn, 'user Own', followUserId, 'id');
	// const followers = await Follower.findOne({ _id: userOwn.followers }).then((result) => {
	// 	return result;
	// });

	const followers = await Follower.findOne({ _id: userOwn.followers }).then((result: any) => {
		const arr: any = [];
		result.following.map((item: any) => {
			if (!arr.includes(item.toString().replace(/ObjectId\("(.*)"\)/, '$1'))) {
				arr.push(item.toString().replace(/ObjectId\("(.*)"\)/, '$1'));
			}
		});
		return arr;
		// .filter((v, i, a) => a.indexOf(v) === i);
	});
	console.log(followers);

	// thi is actually following not followers for the user
	const result = [];

	for (const follower of followers) {
		const user = await User.findOne({ _id: follower })
			.select('_id username posts avatar')
			.populate({
				path: 'posts',
			})
			.exec();
		result.push(user);
	}

	const resultObj: followerObj[] = [];
	result.forEach((data) => {
		const obj: followerObj = {
			userid: data._id,
			username: data.username,
			post: data.posts[data.posts.length - 1],
		};
		resultObj.push(obj);
	});

	res.json({ result: resultObj });
};
//***/git log
