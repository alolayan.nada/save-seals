import User from '../models/users.model';
import Post from '../models/posts.model';
//@ts-ignore
export const post = async (req: any, res: any) => {
	const email = req.body.email;
	const image: String = req.body.image;
	const title: String = req.body.title;
	const description: String = req.body.description;
	const date: Date = req.body.date;

	//get the user name from the email because the email is the key not the user name

	//const arr: String[] = uriPost.split(":");
	//const uri = `${arr[1]}/${arr[2]}`;
	//something to varify
	//@ts-ignore
	const postusername = await User.findOne({ email: email });
	console.log(postusername.username);

	try {
		const post = new Post({
			email: email,
			//@ts-ignore
			username: postusername.username, //.data.username?
			image: image,
			title: title,
			description: description,
			date: date,
		});
		//@ts-ignore
		const result = await post.save();
		//@ts-ignore
		const resultUser = await User.updateOne(
			{ email: email },
			{
				$push: {
					posts: result._id,
				},
			}
		);
		res.json({ result });
	} catch (error: any) {
		console.log('Error:', error);
		res.json({ status: 'errorhhhhh' });
	}
};
interface post extends Document {
	email: any;
	username: any;
	description: any;
}

interface userObj {
	username: string;
	post: any;
}
interface postObjwithAvatar {
	username: string;
	email: string;
	description: string;
	image: string;
	avatar: string;
	title: string;
	date: Date;
	messages: any;
	like: Number;
}

export const getAllPosts = async (req: any, res: any) => {
	const allPosts = await Post.find({}).sort({ _id: -1 }).exec();

	const result: postObjwithAvatar[] = [];

	for (const post of allPosts) {
		//@ts-ignore
		const check = await User.findOne({ email: post.email })
			.then((user: any) => {
				const obj: postObjwithAvatar = {
					//@ts-ignore
					username: post.username,
					//@ts-ignore
					email: post.email,
					//@ts-ignore
					description: post.description,
					//@ts-ignore
					image: post.image,
					avatar: user.avatar,
					//@ts-ignore
					title: post.title,
					//@ts-ignore
					date: post.date.toUTCString(),
					//@ts-ignore
					messages: post.messages,
					//@ts-ignore
					like: post.like,
				};

				result.push(obj);
			})
			.catch((error: any) => console.log(error));
	}

	res.json({ result });
};

export const getOwnPosts = async (req: any, res: any) => {
	const userOwn = res.locals.user;
	const users = await User.find({ email: userOwn.email }) /// users with posts
		.populate({
			path: 'posts',
		})
		.exec();

	console.log(users, 'hghghgh');
	const result: postObjwithAvatar[] = [];

	for (const post of users[0].posts) {
		const obj: postObjwithAvatar = {
			username: post.username,
			email: post.email,
			description: post.description,
			image: post.image,
			avatar: users[0].avatar,
			title: post.title,
			date: post.date.toUTCString(),
			messages: post.messages,
			like: post.like,
		};
		console.log('here', users.avatar);
		result.push(obj);
	}

	// users.forEach((user: any) => {
	// 	if (user.posts.length > 0) {
	// 		const obj: userObj = {
	// 			username: user.username,
	// 			post: user.posts,
	// 		};
	// 		result.push(obj);
	// 	}
	// }); this is needed  if I want to get posts for more than one user
	console.log('result go t', result);
	res.json({ result });
};

interface messageObj {
	username: String;
	message: String;
}

export const getMessages = async (req: any, res: any) => {
	const postId = req.params.postid;
	const results: any = await Post.findOne({ _id: postId })
		.populate({
			path: 'messages',
		})
		.exec();
	const messages = results.messages;
	const result: any = [];
	messages.forEach((message: any) => {
		const obj: messageObj = {
			username: message.email,
			message: message.message,
		};
		result.push(obj);
	});
	res.json({ result });
};

export const increaseLike = async (req: any, res: any) => {
	const postId = req.params.postid;
	const result: any = await Post.findOne({ _id: postId });
	const like = result.like + 1;
	const resultInsert: any = await Post.updateOne(
		{ _id: postId },
		{
			like: like,
		}
	);
	if (resultInsert.ok === 1) {
		res.json({ status: 'ok', like: like });
	} else {
		res.json({ status: 'ng' });
	}
};

export const decreaseLike = async (req: any, res: any) => {
	const postId = req.params.postid;
	const result: any = await Post.findOne({ _id: postId });
	const like = result.like === 0 ? 0 : result.like - 1;
	const resultInsert: any = await Post.updateOne(
		{ _id: postId },
		{
			like: like,
		}
	);
	if (resultInsert.ok === 1) {
		res.json({ status: 'ok', like: like });
	} else {
		res.json({ status: 'ng' });
	}
};
