import express from 'express';
import cors from 'cors';
import mongoose, { ConnectOptions } from 'mongoose';

import path from 'path';
import dotenv from 'dotenv';
dotenv.config();
//export const ATLAS_CONNECTION = process.env.ATLAS_CONNECTION!;
import { verifyJWT, test } from './verifyJWT';
import {
	register,
	login,
	getUsers,
	getOtherUser,
	getIdfromEmail,
	getUserInfo,
} from './controllers/user.controller';
import {
	getOwnPosts,
	getMessages,
	increaseLike,
	decreaseLike,
	post,
	getAllPosts,
} from './controllers/forumPost.controller';
import { addFollower, getOwnFollower } from './controllers/follower.controller';

const app = express();
app.use(express.json()); // To parse the incoming requests with JSON payloads

app.use(cors());

app.post('/register', register);

app.post('/login', login);

//verifyJWT middleware.
//when endpoint is /api/*, always checking JWT.
app.use('/api', verifyJWT);

//exapmle of how to use JWT
//test is a function -> check src/verifyJWT
app.post('/api/test', test);

//get users
app.get('/api/user', getUsers);

//get another user data
app.get('/api/user/:userid', getOtherUser);

app.post('/api/post', post);
app.get('/api/getUserInfo', getUserInfo);
//get own post history
app.get('/api/ownposts', getOwnPosts);
app.get('/getAllPosts', getAllPosts);

//get messages on the post
app.get('/api/message/:postid', getMessages);

//update like's count -> +1
app.patch('/api/like/:postid', increaseLike);

//update like's count -> -1
app.patch('/api/dislike/:postid', decreaseLike);

//add an user to followers list
app.patch('/api/follow/:userid', addFollower);

//get followers list
app.post('/api/followers/:userid', getOwnFollower);

app.post('/api/idfromEmail', getIdfromEmail);
//

mongoose
	.connect(process.env.ATLAS_CONNECTION!, {} as ConnectOptions)
	.then((res) => {
		console.log('Connected ');
	})
	.catch((err) => {
		console.log(` error occured -`, err);
	});

if (process.env.DB_ENVIRONMENT === 'production') {
	// Serve any static files
	app.use(express.static(path.join(__dirname, '../front/build')));

	// Handle React routing, return all requests to React app
	app.get('/*', function (req: any, res: any) {
		res.sendFile(path.join(__dirname, '../front', 'build', 'index.html'));
		//res.sendFile('index.html', { root: '../front-end/build/' });
	});

	const port = process.env.PORT || 4000;

	app.listen(port, () => {
		console.log(`🚀 Server is running on ${port}`);
	});
}
